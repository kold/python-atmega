tests
===========

The script `fake_atmega.py` will pretend to be an atmega. It will connect to a serial tty using 9600 baud.

In order to use it for testing, do the following (in the `test` directory)

Install socat to your linux machine
```
sudo apt-get install socat
```

Create two virtual serial port and connect them
```
socat -d -d pty,raw,echo=0,link=./fake_atmega pty,raw,echo=0,link=client
```

Install serial module to your python3 on your linux machine
```
python3 -m pip install pyserial
```

Start the fake atmega server
```
python3 fake_atmega.py
```

and connect to it using e.g. `minicom` or your own client 
```
minicom -D ./client -b 9600
```
