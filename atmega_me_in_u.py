#!/usr/bin/env python3

import serial
from atmega import read_LED, read_temp, read_button, turn_on_LED, turn_off_LED, blink_LED

if __name__ == '__main__':
    serial_cfg = {  
        "dev": "/dev/serial0",
        "baud": 9600,
    }

def exit_program(serial_connection):
    print("Exited from Program")
    exit()

choicedict = {
    "1" : read_LED,
    "2" : read_temp,
    "3" : read_button,
    "4" : turn_on_LED,
    "5" : turn_off_LED,
    "6" : blink_LED,
    "7" : exit_program,
}


def dict_funct_to_name(key):
    function_name_with_extra = str(choicedict[key])
    function_name_split = function_name_with_extra.split()
    return str(function_name_split[1])


def menu_builder():
    top_strip = 30*"-"+" MENU "+30*"-"
    bottom_strip = 66*"-"
    print(top_strip)
    key_list = list(choicedict)
    sorted_keylist = sorted(key_list)
    for i in sorted_keylist:
        function_name = dict_funct_to_name(i)
        print(i+" - "+function_name)
    print(bottom_strip)


def choice_taker(serial_cfg, choice):
    with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as serial_connection:
        reply = choicedict[choice](serial_connection)
    print(reply)


def main():
    menu_builder()
    while True:
        choice = str(input("Please enter your choice\n> "))
        if choice == "":
            menu_builder()
            continue
        choice_taker(serial_cfg, choice)

if __name__ == '__main__':
    main()