#!flask/bin/python3
from flask import Flask, request, jsonify
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
from atmega import read_temp, read_button, turn_on_LED, turn_off_LED, blink_LED
import serial

app = Flask(__name__)
CORS(app, resources=r'/*') # to allow external resources to fetch data
api = Api(app)

if __name__ == '__main__':
    serial_cfg = {
        "dev": "/dev/serial0",
        "baud": 9600,
    }

    led_state_dict = {
    "on":turn_on_LED,
    "off":turn_off_LED,
    "blink":blink_LED,
    }

@api.resource("/")
class url_index(Resource):
    def get(self):
        print("/")
        with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as serial_connection:
            print(serial_connection)
            temp_value=str(read_temp(serial_connection))
            button_state=str(read_button(serial_connection))
            return jsonify(
                TEMPERATURE=temp_value,
                Button=button_state,
                )

@api.resource("/led1/<state>")
class url_led_toggle(Resource):
    def get(self, state):
        print("/state")
        with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as serial_connection:
            return(led_state_dict[state](serial_connection).split())

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=False)