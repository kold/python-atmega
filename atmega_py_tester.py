#!/usr/bin/env python3

import serial
from atmega import read_LED, read_temp, read_button, turn_on_LED, turn_off_LED, blink_LED, read_ser_line
from atmega_me_in_u import choicedict, choice_taker, exit_program, dict_funct_to_name

serial_cfg = {  
    "dev": "/dev/serial0",
    "baud": 9600,
    }

choicedict = {
    "1" : read_LED,
    "2" : read_temp,
    "3" : read_button,
    "4" : turn_on_LED,
    "5" : turn_off_LED,
    "6" : blink_LED,
    "7" : exit_program,
}

def test_run_through():
    test_list = list(range(1,8))
    for i in test_list:
        print("executing "+dict_funct_to_name(str(i)))
        choice_taker(serial_cfg, str(i))

def main():
    serial_cfg["dev"]="./client"
    test_run_through()

main()